import subprocess as sp
import os
import time
import argparse

CLIENT_PATH = (os.path.join(os.path.dirname(__file__), 'base_client.py'))
SERVER_PATH = (os.path.join(os.path.dirname(__file__), 'base_server.py'))


def run_clients(count):
    clients = []
    for i in range(count):
        clients.append(sp.Popen(CLIENT_PATH, shell=True))
        print(f"running client {i}")
        time.sleep(1)
    return clients


def main():
    parser = argparse.ArgumentParser(description='count of clients')
    parser.add_argument('-c', '--count', required=True)
    args = parser.parse_args()
    main_client = sp.Popen(['sh', CLIENT_PATH], stdout=sp.PIPE, stdin=sp.PIPE, stderr=sp.PIPE)
    clients = run_clients(int(args.count))
    main_client.communicate(input=bytes('test message', encoding='utf8'))
    main_client.kill()
    for client in clients:
        client.kill()


if __name__ == "__main__":
    main()
    # Сервер перестает работать корректно примерно на 20 клиентах
