#!/usr/bin/env python
import socket
import threading
import sys

address_to_server = ("localhost", 8686)

client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client.connect(address_to_server)


def read_messages(client_: socket.socket) -> None:
    while True:
        data = client_.recv(1024)
        sys.stderr.write(str(data))
        if not data:
            break


thread = threading.Thread(target=read_messages, args=(client,))
thread.start()

client.send(bytes("message", encoding="utf8"))